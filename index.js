const express = require('express');
const app = express();

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'Acamica API',
        version: '1.0.0'
      }
    },
    apis: ['./index.js'],
  };
  
  const swaggerDocs = swaggerJsDoc(swaggerOptions);

//EJEMPLO COMPLETO
/**
 * @swagger
 * /estudiantes:
 *  post:
 *    description: Crea un nuevo estudiante
 *    parameters:
 *    - name: nombre
 *      description: Nombre del estudiante
 *      in: formData
 *      required: true
 *      type: string
  *    - name: edad
 *      description: Edad del estudiante
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */
 app.post('/estudiantes', (req, res) => {
    res.status(201).send();
  });

  app.use('/api-docs',
   swaggerUI.serve,
   swaggerUI.setup(swaggerDocs));

   app.listen(3000, () => console.log('Corriendo en el puerto 3000'));