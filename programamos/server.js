const express = require('express');
const server = express();

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

//Configurar los Middleware
//Este método se llama como middleware
server.use(express.json());
//es un método incorporado en express para reconocer el objeto de solicitud entrante como objeto JSON.
//Este método se llama como middleware
server.use(express.urlencoded({ extended: true }));

//CONFIGURAR SWAGGER
const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'Programamos 13ra clase Acamica',
        version: '1.0.0'
      }
    },
    apis: ['./server.js'],
  };

//CONFIGURAR EL SwaggerDocs
  const swaggerDocs = swaggerJsDoc(swaggerOptions);

//CONFIGURAR SERVER PARA EL USO DE SWAGGER, O CONFIGURAR EL ENDPOINT

server.use('/api-docs',
   swaggerUI.serve,
   swaggerUI.setup(swaggerDocs));



  let vechiculos = [
    {
        marca:"Ford",
        modelo:"Ford EcoSport.",
        fecha_fabricacion: new Date(25/10/2000),
        puertas: 4,
        disponible:true
    },
    {
        marca:"Mazda",
        modelo:"Mazda 3.",
        fecha_fabricacion: new Date(2/05/2015),
        puertas: 5,
        disponible:false
    }
];

server.post('/vehiculos', (req,res)=>{
  const {marca, modelo, fecha, puertas, disponible} = req.body;
  const vehiculo = {
    marca: marca,
    modelo: modelo,
    fecha_fabricacion: fecha,
    puertas: puertas,
    disponible: disponible
  }
  vechiculos.push(vehiculo);
  return res.send(vehiculos);
});

server.get('/vehiculos', (req,res)=> res.send(vehiculos))

server.listen(3000, () => console.log('Corriendo en el puerto 3000'));