const express = require('express');
const server = express();

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

//Configurar los Middleware
//Este método se llama como middleware
server.use(express.json());
//es un método incorporado en express para reconocer el objeto de solicitud entrante como objeto JSON.
//Este método se llama como middleware
server.use(express.urlencoded({ extended: true }));

//CONFIGURAR SWAGGER
const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'Challenge 13ra clase Acamica',
        version: '1.0.0'
      }
    },
    apis: ['./server.js'],
  };

//CONFIGURAR EL SwaggerDocs
  const swaggerDocs = swaggerJsDoc(swaggerOptions);

//CONFIGURAR SERVER PARA EL USO DE SWAGGER, O CONFIGURAR EL ENDPOINT

server.use('/api-docs',
   swaggerUI.serve,
   swaggerUI.setup(swaggerDocs));

/**
 * @swagger
 * /estudiantes:
 *  post:
 *    description: Crea un nuevo estudiante
 *    parameters:
 *    - name: nombre
 *      description: Nombre del estudiante
 *      in: formData
 *      required: true
 *      type: string
 *    - name: edad
 *      description: Edad del estudiante
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.post('/estudiantes', (req,res)=>{
    res.status(201).send('Hola estudiantes');
});

/**
 * @swagger
 * /estudiantes:
 *  get:
 *    description: Devuelve la lista de los estudiantes
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.get('/estudiantes', (req,res)=>{
    res.status(201).send('Hola estudiantes');
});

/**
 * @swagger
 * /estudiantes:
 *  delete:
 *    description: Elimina un estudiante
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.delete('/estudiantes', (req,res)=>{
    res.status(201).send('Hola estudiantes');
});

/**
 * @swagger
 * /estudiantes:
 *  patch:
 *    description: Actualiza la información de un estudiante
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.patch('/estudiantes', (req,res)=>{
    res.status(201).send('Hola estudiantes');
});

/**
 * @swagger
 * /estudiantes:
 *  put:
 *    description: Actualiza la información de un estudiante
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.put('/estudiantes', (req,res)=>{
    res.status(201).send('Hola estudiantes');
});

server.listen(3000, ()=> console.log('Servidor corriendo en puerto 3000'));